<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" xmlns:its="http://www.w3.org/2005/11/its" type="guide" id="keyboard-shortcuts" xml:lang="sv">

  <info>
    <link type="guide" xref="index#options"/>

    <revision pkgversion="3.8" date="2013-03-23" status="final"/>
    <revision pkgversion="3.12" date="2014-03-05" status="final"/>

    <credit type="author">
      <name>Ekaterina Gerasimova</name>
      <email its:translate="no">kittykat3756@googlemail.com</email>
    </credit>

    <include xmlns="http://www.w3.org/2001/XInclude" href="legal.xml"/>

    <desc>Inaktivera sändning av tangentbordsgenvägar till fjärrmaskinen.</desc>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Sebastian Rasmussen</mal:name>
      <mal:email>sebras@gmail.com</mal:email>
      <mal:years>2016</mal:years>
    </mal:credit>
  </info>

  <title>Tangentbordsgenvägar</title>

  <p>Du kan aktivera eller inaktivera tangentbordsgenvägar genom att välja <guiseq><gui style="menu">Visa</gui> <gui style="menuitem">Tangentbordsgenvägar</gui></guiseq>.</p>

  <p>Att aktivera denna inställning tillåter användning av tangentbordsgenvägar (så som <keyseq><key>Ctrl</key><key>N</key></keyseq>), menytangenter (så som <keyseq><key>Alt</key><key>M</key></keyseq>) och menyåtkomsttangenter i <app>Fjärrskrivbordsvisare</app>. Detta innebär att <app>Fjärrskrivbordsvisare</app> kommer att fånga de tangentkombinationerna istället för att skicka dem till fjärrmaskinen. Denna inställning är inaktiverad som standard eftersom du mestadels kommer att interagera med datorn som du är ansluten till.</p>

  <note>
    <p>När inställningen <gui>Tangentbordsgenvägar</gui> är inaktiverad kommer <keyseq><key>Ctrl</key><key>Alt</key><key>Delete</key></keyseq> vara den enda tangentkombinationen som inte kommer att skickas till fjärrskrivbordet. Välj <guiseq><gui style="menu">Fjärr</gui> <gui style="menuitem">Skicka Ctrl-Alt-Delete</gui></guiseq>, eller använd motsvarande knapp i verktygsfältet, för att skicka denna tangentkombination.</p>
  </note>

</page>
