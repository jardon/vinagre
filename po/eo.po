# Esperanto translation for vinagre.
# Copyright (c) 2009 Rosetta Contributors and Canonical Ltd 2009
# This file is distributed under the same license as the vinagre package.
# Kim RIBEIRO <ribekim@gmail.com>, 2009.
# Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>, 2010, 2011, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: vinagre\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=vinagre&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2012-01-02 15:25+0000\n"
"PO-Revision-Date: 2012-03-31 07:07+0200\n"
"Last-Translator: Kristjan SCHMIDT <kristjan.schmidt@googlemail.com>\n"
"Language-Team: Esperanto <gnome-l10n-eo@lists.launchpad.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: eo\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"X-Launchpad-Export-Date: 2011-05-09 17:08+0000\n"
"X-Generator: Launchpad (build 12981)\n"

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:1
msgid "Maximum number of history items in connect dialog"
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:2
msgid ""
"Set to \"false\" to disable menu shortcuts. Set to \"true\" to enable them. "
"Note that if they are enabled, those keys will be intercepted by the menu "
"and will not be sent to the remote host."
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:3
msgid ""
"Set to \"true\" to always show the tabs. Set to \"false\" to only show the "
"tabs when there is more than one active connection."
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:4
msgid ""
"Set to \"true\" to always start the program listening for reverse "
"connections."
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:5
msgid "Specifies the maximum number of items in the host dropdown entry."
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:6
msgid ""
"When connecting to a host, the client can say to the server to leave other "
"clients connected or to drop the existent connections. Set the value to true "
"to share the desktop with the other clients."
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:7
msgid "Whether we should leave other clients connected"
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:8
msgid ""
"Whether we should show tabs even when there is only one active connection"
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:9
msgid "Whether we should show the menu accelerators (shortcut keys)"
msgstr ""

#: ../data/org.gnome.Vinagre.gschema.xml.in.h:10
msgid "Whether we should start the program listening for reverse connections"
msgstr ""

#: ../data/vinagre.desktop.in.in.h:1 ../data/vinagre-file.desktop.in.in.h:1
msgid "Access remote desktops"
msgstr "Aliro al foraj labortabloj"

#: ../data/vinagre.desktop.in.in.h:2 ../data/vinagre-file.desktop.in.in.h:2
#: ../vinagre/vinagre-main.c:129
msgid "Remote Desktop Viewer"
msgstr "Vidigilo de foraj labortabloj"

#: ../data/vinagre.ui.h:1
msgid "Authentication is required"
msgstr "Aŭtentokontrolo estas bezonata"

#: ../data/vinagre.ui.h:2
msgid "Bookmark Folder"
msgstr "Legosigno-dosierujo"

#: ../data/vinagre.ui.h:3
msgid "Bookmarks"
msgstr "Legosignoj"

#: ../data/vinagre.ui.h:4
msgid ""
"By activating reverse connections you can access remote desktops that are "
"behind a firewall. The remote side is supposed to initiate the connection "
"with you. For further information, read the help."
msgstr ""

#: ../data/vinagre.ui.h:5
msgid "Choose a remote desktop to connect to"
msgstr "Elekti foran labortablon al kiu konekti"

#: ../data/vinagre.ui.h:6
msgid "Connect"
msgstr "Konekti"

#: ../data/vinagre.ui.h:7
msgid "Connection"
msgstr "Konekto"

#: ../data/vinagre.ui.h:8
msgid "Connection options"
msgstr "Konektagordoj"

#: ../data/vinagre.ui.h:9
msgid "Connectivity"
msgstr "Konekteco"

#: ../data/vinagre.ui.h:10
msgid "Enable fullscreen mode for this connection"
msgstr "Enŝalti tutekranan reĝimon por tiu konekto"

#: ../data/vinagre.ui.h:11
msgid "Folder"
msgstr "Dosierujo"

#: ../data/vinagre.ui.h:12 ../plugins/rdp/vinagre-rdp-tab.c:54
#: ../plugins/ssh/vinagre-ssh-tab.c:49 ../plugins/vnc/vinagre-vnc-tab.c:149
#: ../plugins/spice/vinagre-spice-tab.c:139
msgid "Host:"
msgstr "Gastiganto:"

#: ../data/vinagre.ui.h:13
msgid "Options"
msgstr "Agordoj"

#: ../data/vinagre.ui.h:14
msgid "Parent Folder"
msgstr "Gepatra dosierujo"

#: ../data/vinagre.ui.h:15
msgid "Reverse Connections"
msgstr ""

#: ../data/vinagre.ui.h:16
msgid "Search for remote hosts on the network"
msgstr ""

#: ../data/vinagre.ui.h:17
msgid "Select a remote desktop protocol for this connection"
msgstr ""

#: ../data/vinagre.ui.h:18
msgid "Store the login credential in GNOME Keyring"
msgstr ""

#: ../data/vinagre.ui.h:19
msgid "This desktop is reachable through the following IP address(es):"
msgstr ""

#: ../data/vinagre.ui.h:20
msgid "Vinagre is a remote desktop viewer for the GNOME desktop"
msgstr ""

#. Translators: this is the reverse connection mode. "Always enabled" means it will be enabled by default in the program startup. You can see this string in the dialog Remote->Reverse connections.
#: ../data/vinagre.ui.h:22
msgid "_Always Enabled"
msgstr "Ĉi_am enŝaltite"

#. This is a button label, in the authentication dialog
#: ../data/vinagre.ui.h:24
msgid "_Authenticate"
msgstr "_Aŭtentigi"

#: ../data/vinagre.ui.h:25
msgid "_Enable Reverse Connections"
msgstr ""

#: ../data/vinagre.ui.h:26
msgid "_Full screen"
msgstr "_Tuta ekrano"

#: ../data/vinagre.ui.h:27
msgid "_Fullscreen"
msgstr "_Tutekrane"

#: ../data/vinagre.ui.h:28
msgid "_Host:"
msgstr "_Gastiganto:"

#. Translators: This is the name of a bookmark. It appears in the add/edit bookmark dialog.
#: ../data/vinagre.ui.h:30
msgid "_Name:"
msgstr "_Nomo:"

#: ../data/vinagre.ui.h:31 ../plugins/spice/vinagre-spice-plugin.c:294
msgid "_Password:"
msgstr "_Pasvorto:"

#: ../data/vinagre.ui.h:32
msgid "_Protocol:"
msgstr "_Protokolo:"

#: ../data/vinagre.ui.h:33
msgid "_Remember this credential"
msgstr ""

#: ../data/vinagre.ui.h:34 ../plugins/rdp/vinagre-rdp-plugin.c:105
#: ../plugins/ssh/vinagre-ssh-plugin.c:110
msgid "_Username:"
msgstr "_Uzantonomo:"

#. Add your name here to appear as a translator in the about dialog
#: ../data/vinagre.ui.h:36
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Kristjan SCHMIDT https://launchpad.net/~kristjan-eo"

#: ../data/vinagre-mime.xml.in.h:1
msgid "Remote Desktop (VNC) file"
msgstr ""

#: ../data/vinagre-mime.xml.in.h:2
msgid "Remote Desktop Connection"
msgstr ""

#: ../plugins/rdp/vinagre-rdp-plugin.c:55
msgid "RDP"
msgstr ""

#. Translators: This is a description of the RDP protocol. It appears in the Connect dialog.
#: ../plugins/rdp/vinagre-rdp-plugin.c:57
msgid "Access MS Windows remote desktops"
msgstr ""

#: ../plugins/rdp/vinagre-rdp-plugin.c:96
msgid "RDP Options"
msgstr "RDP-agordoj"

#. Translators: This is the tooltip for the username field in a RDP connection
#. Translators: This is the tooltip for the username field in a SSH connection
#: ../plugins/rdp/vinagre-rdp-plugin.c:110
#: ../plugins/ssh/vinagre-ssh-plugin.c:115
msgid ""
"Optional. If blank, your username will be used. Also, it can be supplied in "
"the Host field above, in the form username@hostname."
msgstr ""

#: ../plugins/rdp/vinagre-rdp-tab.c:55 ../plugins/ssh/vinagre-ssh-tab.c:50
#: ../plugins/vnc/vinagre-vnc-tab.c:150
#: ../plugins/spice/vinagre-spice-tab.c:140
msgid "Port:"
msgstr "Pordo:"

#: ../plugins/rdp/vinagre-rdp-tab.c:124
msgid "Error while executing xfreerdp"
msgstr ""

#: ../plugins/rdp/vinagre-rdp-tab.c:125 ../vinagre/vinagre-bookmarks.c:366
#: ../vinagre/vinagre-bookmarks.c:492
#: ../vinagre/vinagre-bookmarks-migration.c:135
#: ../vinagre/vinagre-cache-prefs.c:57 ../vinagre/vinagre-commands.c:163
#: ../vinagre/vinagre-connect.c:495 ../vinagre/vinagre-options.c:82
#: ../vinagre/vinagre-options.c:100 ../vinagre/vinagre-window.c:260
#: ../vinagre/vinagre-window.c:798
msgid "Unknown error"
msgstr "Nekonata eraro"

#: ../plugins/ssh/vinagre-ssh-plugin.c:59
msgid "SSH"
msgstr "SSH"

#. Translators: This is a description of the SSH protocol. It appears at Connect dialog.
#: ../plugins/ssh/vinagre-ssh-plugin.c:61
msgid "Access Unix/Linux terminals"
msgstr ""

#: ../plugins/ssh/vinagre-ssh-plugin.c:101
msgid "SSH Options"
msgstr "SSH-opcioj"

#. Translators: 'shared' here is a VNC protocol specific flag. You can translate it, but I think it's better to let it untranslated
#: ../plugins/vnc/vinagre-vnc-connection.c:317
#: ../plugins/vnc/vinagre-vnc-plugin.c:199
#, c-format
msgid ""
"Bad value for 'shared' flag: %d. It is supposed to be 0 or 1. Ignoring it."
msgstr ""

#. Translators: this is a command line option (run vinagre --help)
#: ../plugins/vnc/vinagre-vnc-plugin.c:50
msgid "Enable scaled mode"
msgstr ""

#. Translators: this is a command line option (run vinagre --help)
#: ../plugins/vnc/vinagre-vnc-plugin.c:63
msgid "VNC Options:"
msgstr "VNC-opcioj"

#. Translators: this is a command line option (run vinagre --help)
#: ../plugins/vnc/vinagre-vnc-plugin.c:65
msgid "Show VNC Options"
msgstr "Montri VNC-opciojn"

#: ../plugins/vnc/vinagre-vnc-plugin.c:87
msgid "VNC"
msgstr "VNC"

#: ../plugins/vnc/vinagre-vnc-plugin.c:88
msgid "Access Unix/Linux, Windows and other remote desktops."
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:145
#: ../plugins/spice/vinagre-spice-plugin.c:100
msgid "Could not parse the file."
msgstr "Ne eblis analizi la dosieron."

#. Translators: Do not translate "Connection". It's the name of a group in the .vnc (.ini like) file.
#: ../plugins/vnc/vinagre-vnc-plugin.c:153
msgid "The file is not a VNC one: Missing the group \"Connection\"."
msgstr ""

#. Translators: Do not translate "Host". It's the name of a key in the .vnc (.ini like) file.
#: ../plugins/vnc/vinagre-vnc-plugin.c:160
msgid "The file is not a VNC one: Missing the key \"Host\"."
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:251
msgid "VNC Options"
msgstr "VNC-opcioj"

#. View only check button
#. View only check button - not fully ready on spice-gtk side
#: ../plugins/vnc/vinagre-vnc-plugin.c:259
#: ../plugins/vnc/vinagre-vnc-tab.c:619
#: ../plugins/spice/vinagre-spice-plugin.c:221
#: ../plugins/spice/vinagre-spice-tab.c:507
msgid "_View only"
msgstr ""

#. Scaling check button
#: ../plugins/vnc/vinagre-vnc-plugin.c:268
#: ../plugins/spice/vinagre-spice-plugin.c:245
msgid "_Scaling"
msgstr "_Skalado"

#. Keep ratio check button
#: ../plugins/vnc/vinagre-vnc-plugin.c:282
msgid "_Keep aspect ratio"
msgstr ""

#. JPEG Compression check button
#: ../plugins/vnc/vinagre-vnc-plugin.c:292
msgid "_Use JPEG Compression"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:293
msgid "This might not work on all VNC servers"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:303
msgid "Color _Depth:"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:308
msgid "Use Server Settings"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:309
msgid "True Color (24 bits)"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:310
msgid "High Color (16 bits)"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:311
msgid "Low Color (8 bits)"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:312
msgid "Ultra Low Color (3 bits)"
msgstr ""

#. Translators: the whole sentence will be: Use Host <hostname> as a SSH tunnel
#. Translators: the whole sentence will be: Use host <hostname> as a SSH tunnel
#: ../plugins/vnc/vinagre-vnc-plugin.c:326
#: ../plugins/spice/vinagre-spice-plugin.c:256
msgid "Use h_ost"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:335
#: ../plugins/spice/vinagre-spice-plugin.c:265
msgid "hostname or user@hostname"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:336
#: ../plugins/spice/vinagre-spice-plugin.c:266
msgid "Supply an alternative port using colon"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-plugin.c:337
#: ../plugins/spice/vinagre-spice-plugin.c:267
msgid "For instance: joe@example.com:5022"
msgstr ""

#. Translators: the whole sentence will be: Use host <hostname> as a SSH tunnel
#: ../plugins/vnc/vinagre-vnc-plugin.c:343
#: ../plugins/spice/vinagre-spice-plugin.c:273
msgid "as a SSH tunnel"
msgstr ""

#. Translators: this is a pattern to open *.vnc files in a open dialog.
#: ../plugins/vnc/vinagre-vnc-plugin.c:378
msgid "VNC Files"
msgstr "VNC-dosieroj"

#: ../plugins/vnc/vinagre-vnc-tab.c:148
msgid "Desktop Name:"
msgstr "Labortabla nomo"

#: ../plugins/vnc/vinagre-vnc-tab.c:151
msgid "Dimensions:"
msgstr "Dimensioj:"

#: ../plugins/vnc/vinagre-vnc-tab.c:296
#: ../plugins/spice/vinagre-spice-tab.c:252
msgid "Error creating the SSH tunnel"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:297 ../plugins/vnc/vinagre-vnc-tab.c:308
#: ../plugins/spice/vinagre-spice-tab.c:253
#: ../plugins/spice/vinagre-spice-tab.c:265
msgid "Unknown reason"
msgstr "Nekonata kialo"

#: ../plugins/vnc/vinagre-vnc-tab.c:307
#: ../plugins/spice/vinagre-spice-tab.c:264
msgid "Error connecting to host."
msgstr ""

#. Translators: %s is a host name or IP address; %u is a code error (number).
#: ../plugins/vnc/vinagre-vnc-tab.c:352
#, c-format
msgid "Authentication method for host %s is unsupported. (%u)"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:356
msgid "Authentication unsupported"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:518 ../plugins/vnc/vinagre-vnc-tab.c:535
msgid "Authentication error"
msgstr "Aŭtentig-eraro"

#: ../plugins/vnc/vinagre-vnc-tab.c:519
msgid "A username is required in order to access this remote desktop."
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:536
msgid "A password is required in order to access this remote desktop."
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:590
#: ../plugins/spice/vinagre-spice-tab.c:466
msgid "S_caling"
msgstr "S_kalado"

#: ../plugins/vnc/vinagre-vnc-tab.c:591
#: ../plugins/spice/vinagre-spice-tab.c:467
msgid "Fits the remote screen into the current window size"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:604
msgid "_Keep Aspect Ratio"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:605
msgid "Keeps the screen aspect ratio when using scaling"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:620
#: ../plugins/spice/vinagre-spice-tab.c:508
msgid "Does not send mouse and keyboard events"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:633
msgid "_Original size"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:634
msgid "Adjusts the window to the remote desktop's size"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:647
msgid "_Refresh Screen"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:648
msgid "Requests an update of the screen"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:670
#: ../plugins/spice/vinagre-spice-tab.c:531
msgid "_Send Ctrl-Alt-Del"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:671
#: ../plugins/spice/vinagre-spice-tab.c:532
msgid "Sends Ctrl+Alt+Del to the remote desktop"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:724 ../plugins/vnc/vinagre-vnc-tab.c:725
#: ../plugins/spice/vinagre-spice-tab.c:584
#: ../plugins/spice/vinagre-spice-tab.c:585
msgid "Scaling"
msgstr "Skalado"

#: ../plugins/vnc/vinagre-vnc-tab.c:734 ../plugins/vnc/vinagre-vnc-tab.c:735
#: ../plugins/spice/vinagre-spice-tab.c:594
#: ../plugins/spice/vinagre-spice-tab.c:595
msgid "Read only"
msgstr "Nurlege"

#. Send Ctrl-alt-del
#: ../plugins/vnc/vinagre-vnc-tab.c:743 ../plugins/vnc/vinagre-vnc-tab.c:745
#: ../plugins/spice/vinagre-spice-tab.c:603
#: ../plugins/spice/vinagre-spice-tab.c:605
msgid "Send Ctrl-Alt-Del"
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tab.c:908
msgid ""
"Scaling is not supported on this installation.\n"
"\n"
"Read the README file (shipped with Vinagre) in order to know how to enable "
"this feature."
msgstr ""

#: ../plugins/vnc/vinagre-vnc-tunnel.c:97
#: ../plugins/spice/vinagre-spice-tunnel.c:103
#, c-format
msgid "Unable to find a free TCP port"
msgstr ""

#. Translators: Do not translate "connection". It's the name of a group in the .spice (.ini like) file.
#: ../plugins/spice/vinagre-spice-plugin.c:107
msgid "The file is not a Spice one: Missing the group \"connection\"."
msgstr ""

#. Translators: Do not translate "host". It's the name of a key in the .spice (.ini like) file.
#: ../plugins/spice/vinagre-spice-plugin.c:114
msgid "The file is not a Spice one: Missing the key \"host\"."
msgstr ""

#: ../plugins/spice/vinagre-spice-plugin.c:158
msgid "SPICE"
msgstr ""

#. Translators: This is a description of the SPICE protocol. It appears at Connect dialog.
#: ../plugins/spice/vinagre-spice-plugin.c:160
msgid "Access Spice desktop server"
msgstr ""

#: ../plugins/spice/vinagre-spice-plugin.c:208
msgid "SPICE Options"
msgstr ""

#. Resize guest check button
#: ../plugins/spice/vinagre-spice-plugin.c:229
#: ../plugins/spice/vinagre-spice-tab.c:480
msgid "_Resize guest"
msgstr ""

#. Clipboard sharing check button
#: ../plugins/spice/vinagre-spice-plugin.c:237
#: ../plugins/spice/vinagre-spice-tab.c:493
msgid "_Share clipboard"
msgstr ""

#. Translators: This is the tooltip for the password field in a SPICE connection
#: ../plugins/spice/vinagre-spice-plugin.c:299
msgid "Optional"
msgstr "Nedevige"

#. Translators: this is a pattern to open *.spice files in a open dialog.
#: ../plugins/spice/vinagre-spice-plugin.c:320
msgid "Spice Files"
msgstr ""

#: ../plugins/spice/vinagre-spice-tab.c:481
msgid "Resize the screen guest to best fit"
msgstr ""

#: ../plugins/spice/vinagre-spice-tab.c:494
msgid "Automatically share clipboard between client and guest"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:366
#, c-format
msgid "Error while initializing bookmarks: %s"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:373
msgid "Error while initializing bookmarks: The file seems to be empty"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:380
msgid ""
"Error while initializing bookmarks: The file is not a vinagre bookmarks file"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:453 ../vinagre/vinagre-bookmarks.c:460
msgid "Error while saving bookmarks: Failed to create the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:467 ../vinagre/vinagre-bookmarks.c:474
msgid "Error while saving bookmarks: Failed to initialize the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:483
msgid "Error while saving bookmarks: Failed to finalize the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks.c:492
#, c-format
msgid "Error while saving bookmarks: %s"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:95
#: ../vinagre/vinagre-bookmarks-migration.c:102
msgid "Error while migrating bookmarks: Failed to create the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:109
#: ../vinagre/vinagre-bookmarks-migration.c:116
msgid "Error while migrating bookmarks: Failed to initialize the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:125
msgid "Error while migrating bookmarks: Failed to finalize the XML structure"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:135
#: ../vinagre/vinagre-bookmarks-migration.c:216
#, c-format
msgid "Error while migrating bookmarks: %s"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:164
msgid "Error while migrating bookmarks: VNC plugin is not activated"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:216
msgid "Failed to create the directory"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:241
msgid ""
"Migrating the bookmarks file to the new format. This operation is only "
"supposed to run once."
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:249
#, c-format
msgid "Error opening old bookmarks file: %s"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:250
#: ../vinagre/vinagre-bookmarks-migration.c:262
msgid "Migration cancelled"
msgstr ""

#: ../vinagre/vinagre-bookmarks-migration.c:259
msgid "Could not remove the old bookmarks file"
msgstr "Ne eblas forigi la malnovan legosigno-dosieron"

#: ../vinagre/vinagre-bookmarks-tree.c:120
msgid "Root Folder"
msgstr "Radika dosierujo"

#: ../vinagre/vinagre-bookmarks-ui.c:78
msgid "Invalid name for this folder"
msgstr "Nevalida nomo por tiu dosierujo"

#: ../vinagre/vinagre-bookmarks-ui.c:88 ../vinagre/vinagre-bookmarks-ui.c:200
#, c-format
msgid ""
"The name \"%s\" is already used in this folder. Please use a different name."
msgstr ""
"La nomo \"%s\" jam estas uzata en ĉi tiu dosierujo. Bonvolu uzi alian nomon."

#: ../vinagre/vinagre-bookmarks-ui.c:89 ../vinagre/vinagre-bookmarks-ui.c:190
#: ../vinagre/vinagre-bookmarks-ui.c:201
msgid "Invalid name for this item"
msgstr "Nevalida nomo por tiu elemento"

#. Translators: %s is a protocol name, like VNC or SSH
#: ../vinagre/vinagre-bookmarks-ui.c:168
#, c-format
msgid "(Protocol: %s)"
msgstr "(Protokolo: %s)"

#. Translators: %s is a bookmark entry name
#: ../vinagre/vinagre-bookmarks-ui.c:306
#, c-format
msgid "Are you sure you want to remove %s from bookmarks?"
msgstr "Ĉu vi certe volas forigi na %s de legosignoj?"

#: ../vinagre/vinagre-bookmarks-ui.c:312
msgid "Remove Folder?"
msgstr "Ĉu forigi dosierujon?"

#: ../vinagre/vinagre-bookmarks-ui.c:313
msgid "Note that all its subfolders and bookmarks will be removed as well."
msgstr ""

#: ../vinagre/vinagre-bookmarks-ui.c:318
msgid "Remove Item?"
msgstr "Ĉu forigi eron?"

#: ../vinagre/vinagre-bookmarks-ui.c:335
msgid "Error removing bookmark: Entry not found"
msgstr ""

#: ../vinagre/vinagre-bookmarks-ui.c:352
msgid "New Folder"
msgstr "Nova dosierujo"

#: ../vinagre/vinagre-cache-prefs.c:57
#, c-format
msgid "Error while saving preferences: %s"
msgstr ""

#: ../vinagre/vinagre-commands.c:115
msgid "Choose the file"
msgstr "Elektu la dosieron"

#: ../vinagre/vinagre-commands.c:139
msgid "There are no supported files"
msgstr ""

#: ../vinagre/vinagre-commands.c:140
msgid ""
"None of the active plugins support this action. Activate some plugins and "
"try again."
msgstr ""

#: ../vinagre/vinagre-commands.c:174
msgid "The following file could not be opened:"
msgid_plural "The following files could not be opened:"
msgstr[0] ""
msgstr[1] ""

#: ../vinagre/vinagre-connect.c:90 ../vinagre/vinagre-connect.c:336
#: ../vinagre/vinagre-connect.c:459
msgid "Could not get the active protocol from the protocol list."
msgstr ""

#: ../vinagre/vinagre-connect.c:317
#, c-format
msgid "Error while saving history file: %s"
msgstr ""

#: ../vinagre/vinagre-connect.c:352
msgid "Choose a Remote Desktop"
msgstr "Elektu foran labortablon"

#: ../vinagre/vinagre-connection.c:525 ../vinagre/vinagre-tab.c:575
#: ../vinagre/vinagre-tube-handler.c:257
#, c-format
msgid "The protocol %s is not supported."
msgstr "La protokolo '%s' ne estas subtenata."

#: ../vinagre/vinagre-connection.c:633
msgid "Could not open the file."
msgstr "Ne eblis malfermi la dosieron."

#: ../vinagre/vinagre-connection.c:657
msgid "The file was not recognized by any of the plugins."
msgstr ""

#. Setup command line options
#: ../vinagre/vinagre-main.c:133
msgid "- Remote Desktop Viewer"
msgstr "- Vidigilo de foraj labortabloj"

#: ../vinagre/vinagre-mdns.c:172
#, c-format
msgid "Failed to resolve avahi hostname: %s\n"
msgstr ""

#: ../vinagre/vinagre-mdns.c:226
#, c-format
msgid "The service %s was already registered by another plugin."
msgstr ""

#: ../vinagre/vinagre-mdns.c:234
#, c-format
msgid "Failed to add mDNS browser for service %s."
msgstr ""

#. Translators: "Browse for hosts" means the ability to find/locate some remote hosts [with the VNC service enabled] in the local network
#: ../vinagre/vinagre-mdns.c:251
#, c-format
msgid "Failed to browse for hosts: %s\n"
msgstr ""

#: ../vinagre/vinagre-mdns.c:320
#, c-format
msgid "Failed to initialize mDNS browser: %s\n"
msgstr ""

#. Translators: %s is a host name or IP address.
#: ../vinagre/vinagre-notebook.c:462
#, c-format
msgid "Connection to host %s was closed."
msgstr ""

#: ../vinagre/vinagre-notebook.c:464
msgid "Connection closed"
msgstr "Konekto fermiĝis"

#. Translators: %s is a host name or IP address.
#: ../vinagre/vinagre-notebook.c:483
#, c-format
msgid "Authentication for host %s has failed"
msgstr "Aŭtentokontrolo por gastiganto %s fiaskis"

#: ../vinagre/vinagre-notebook.c:489
msgid "Authentication failed"
msgstr "Aŭtentokontrolo fiaskis"

#: ../vinagre/vinagre-notebook.c:537
#| msgid "Connection"
msgid "Connecting…"
msgstr "Konektado…"

#: ../vinagre/vinagre-notebook.c:559
msgid "Close connection"
msgstr "Fermi konekton"

#. Translators: this is a command line option (run vinagre --help)
#: ../vinagre/vinagre-options.c:33
msgid "Specify geometry of the main Vinagre window"
msgstr ""

#. Translators: this is a command line option (run vinagre --help)
#: ../vinagre/vinagre-options.c:37
msgid "Open Vinagre in fullscreen mode"
msgstr ""

#. Translators: this is a command line option (run vinagre --help)
#: ../vinagre/vinagre-options.c:41
msgid "Create a new toplevel window in an existing instance of Vinagre"
msgstr "Krei novan supranivelan fenestron en ekzistanta ekzemplo de Vinagreo"

#. Translators: this is a command line option (run vinagre --help)
#: ../vinagre/vinagre-options.c:45
msgid "Open a file recognized by Vinagre"
msgstr ""

#: ../vinagre/vinagre-options.c:45
msgid "filename"
msgstr "dosiernomo"

#. Translators: this is a command line option (run vinagre --help)
#: ../vinagre/vinagre-options.c:50
msgid "[server:port]"
msgstr ""

#: ../vinagre/vinagre-options.c:124
#, c-format
msgid "Invalid argument %s for --geometry"
msgstr ""

#: ../vinagre/vinagre-options.c:142
msgid "The following error has occurred:"
msgid_plural "The following errors have occurred:"
msgstr[0] "La jena eraro okazis:"
msgstr[1] "La jenaj eraroj okazis:"

#: ../vinagre/vinagre-prefs.c:83
msgid "Cannot initialize preferences manager."
msgstr "Ne eblas pravalorizi agordoadministrilon."

#: ../vinagre/vinagre-reverse-vnc-listener-dialog.c:103
msgid "IPv4:"
msgstr "IPv4:"

#: ../vinagre/vinagre-reverse-vnc-listener-dialog.c:115
msgid ""
"\n"
"\n"
"IPv6:"
msgstr ""
"\n"
"\n"
"IPv6:"

#: ../vinagre/vinagre-reverse-vnc-listener-dialog.c:175
#, c-format
msgid "On the port %d"
msgstr ""

#: ../vinagre/vinagre-reverse-vnc-listener.c:212
msgid "Error activating reverse connections"
msgstr ""

#: ../vinagre/vinagre-reverse-vnc-listener.c:213
msgid ""
"The program could not find any available TCP ports starting at 5500. Is "
"there any other running program consuming all your TCP ports?"
msgstr ""

#: ../vinagre/vinagre-ssh.c:115
msgid "Timed out when logging into SSH host"
msgstr ""

#: ../vinagre/vinagre-ssh.c:191
msgid "Unable to spawn ssh program"
msgstr ""

#: ../vinagre/vinagre-ssh.c:208
#, c-format
msgid "Unable to spawn ssh program: %s"
msgstr ""

#: ../vinagre/vinagre-ssh.c:425
msgid "Timed out when logging in"
msgstr ""

#: ../vinagre/vinagre-ssh.c:455 ../vinagre/vinagre-ssh.c:603
#: ../vinagre/vinagre-ssh.c:687
msgid "Permission denied"
msgstr "Permeso rifuzita"

#: ../vinagre/vinagre-ssh.c:514
msgid "Password dialog canceled"
msgstr ""

#: ../vinagre/vinagre-ssh.c:537
msgid "Could not send password"
msgstr ""

#: ../vinagre/vinagre-ssh.c:545
msgid "Log In Anyway"
msgstr "Ensaluti malgraŭe"

#: ../vinagre/vinagre-ssh.c:545
msgid "Cancel Login"
msgstr "Ĉesi ensaluton"

#: ../vinagre/vinagre-ssh.c:554
#, c-format
msgid ""
"The identity of the remote host (%s) is unknown.\n"
"This happens when you log in to a host the first time.\n"
"\n"
"The identity sent by the remote host is %s. If you want to be absolutely "
"sure it is safe to continue, contact the system administrator."
msgstr ""

#: ../vinagre/vinagre-ssh.c:572
msgid "Login dialog canceled"
msgstr ""

#: ../vinagre/vinagre-ssh.c:593
msgid "Can't send host identity confirmation"
msgstr ""

#: ../vinagre/vinagre-ssh.c:628 ../vinagre/vinagre-tab.c:808
msgid "Error saving the credentials on the keyring."
msgstr ""

#: ../vinagre/vinagre-ssh.c:694
msgid "Hostname not known"
msgstr ""

#: ../vinagre/vinagre-ssh.c:701
msgid "No route to host"
msgstr ""

#: ../vinagre/vinagre-ssh.c:708
msgid "Connection refused by server"
msgstr ""

#: ../vinagre/vinagre-ssh.c:715
msgid "Host key verification failed"
msgstr ""

#: ../vinagre/vinagre-ssh.c:752
msgid "Unable to find a valid SSH program"
msgstr ""

#: ../vinagre/vinagre-tab.c:354
msgid "Disconnect"
msgstr "Malkonekti"

#: ../vinagre/vinagre-tab.c:375
msgid "Leave fullscreen"
msgstr "Forlasi tutekranan reĝimon"

#: ../vinagre/vinagre-tab.c:537
msgid "Error saving recent connection."
msgstr ""

#: ../vinagre/vinagre-tab.c:898
msgid "Could not get a screenshot of the connection."
msgstr ""

#: ../vinagre/vinagre-tab.c:903
msgid "Save Screenshot"
msgstr "Konservi ekrankopion"

#: ../vinagre/vinagre-tab.c:917
#, c-format
#| msgid "Screenshot of %s"
msgid "Screenshot of %s at %s"
msgstr "Ekrankopio de %s sur %s"

#: ../vinagre/vinagre-tab.c:971
msgid "Error saving screenshot"
msgstr "Eraro dum konservado de ekrankopio"

#: ../vinagre/vinagre-tube-handler.c:233
#, c-format
msgid "Impossible to get service property: %s"
msgstr ""

#: ../vinagre/vinagre-tube-handler.c:300
#, c-format
msgid "Impossible to create the connection: %s"
msgstr ""

#: ../vinagre/vinagre-tube-handler.c:339
#, c-format
msgid "Impossible to accept the stream tube: %s"
msgstr ""

#. Translators: this is an error message when we fail to get the name of an empathy/telepathy buddy. %s will be replaced by the actual error message.
#: ../vinagre/vinagre-tube-handler.c:477
#, c-format
msgid "Impossible to get the contact name: %s"
msgstr ""

#: ../vinagre/vinagre-tube-handler.c:514
#, c-format
msgid "Impossible to get the avatar: %s"
msgstr ""

#: ../vinagre/vinagre-tube-handler.c:535
#, c-format
msgid "%s wants to share their desktop with you."
msgstr ""

#: ../vinagre/vinagre-tube-handler.c:540
msgid "Desktop sharing invitation"
msgstr ""

#. Toplevel
#: ../vinagre/vinagre-ui.h:33
msgid "_Remote"
msgstr "De_fore"

#: ../vinagre/vinagre-ui.h:34
msgid "_Edit"
msgstr "R_edakti"

#: ../vinagre/vinagre-ui.h:35
msgid "_View"
msgstr "_Vido"

#: ../vinagre/vinagre-ui.h:36
msgid "_Bookmarks"
msgstr "_Legosignoj"

#: ../vinagre/vinagre-ui.h:37
msgid "_Help"
msgstr "_Helpo"

#: ../vinagre/vinagre-ui.h:41
msgid "Connect to a remote desktop"
msgstr "Konekti al fora labortablo"

#: ../vinagre/vinagre-ui.h:43
msgid "Open a .VNC file"
msgstr "Malfermi .VNC-dosieron"

#. Translators: "Reverse" here is an adjective, not a verb.
#: ../vinagre/vinagre-ui.h:45
msgid "_Reverse Connections…"
msgstr ""

#: ../vinagre/vinagre-ui.h:45
msgid "Configure incoming VNC connections"
msgstr ""

#: ../vinagre/vinagre-ui.h:48
msgid "Quit the program"
msgstr "Ĉesi la programon"

#. Help menu
#: ../vinagre/vinagre-ui.h:51
msgid "_Contents"
msgstr "_Enhavoj"

#: ../vinagre/vinagre-ui.h:52
msgid "Open the Vinagre manual"
msgstr "Malfermi la manlibron de Vinagreo"

#: ../vinagre/vinagre-ui.h:54
msgid "About this application"
msgstr "Pri ĉi tiu aplikaĵo"

#: ../vinagre/vinagre-ui.h:59
msgid "_Keyboard shortcuts"
msgstr "_Klavkombinoj"

#: ../vinagre/vinagre-ui.h:60
msgid "Enable keyboard shurtcuts"
msgstr "Enŝalti klavkombinojn"

#: ../vinagre/vinagre-ui.h:62
msgid "_Toolbar"
msgstr "Ilobre_to"

#: ../vinagre/vinagre-ui.h:63
msgid "Show or hide the toolbar"
msgstr "Montri aŭ kaŝi la ilobreton"

#: ../vinagre/vinagre-ui.h:66
msgid "_Statusbar"
msgstr "_Statobreto"

#: ../vinagre/vinagre-ui.h:67
msgid "Show or hide the statusbar"
msgstr "Montri aŭ kaŝi la statobreton"

#: ../vinagre/vinagre-ui.h:75
msgid "Disconnect the current connection"
msgstr "Malkonekti la aktualan konekton"

#: ../vinagre/vinagre-ui.h:76
msgid "Disconnect All"
msgstr "Malkonekti ĉiujn"

#: ../vinagre/vinagre-ui.h:77
msgid "Disconnect all connections"
msgstr "Malkonekti ĉiujn konektojn"

#. Bookmarks menu
#: ../vinagre/vinagre-ui.h:80
msgid "_Add Bookmark"
msgstr "_Aldoni legosignon"

#: ../vinagre/vinagre-ui.h:81
msgid "Add the current connection to your bookmarks"
msgstr "Aldoni la aktualan konekton al viaj legosignoj"

#. Remote menu
#: ../vinagre/vinagre-ui.h:88
msgid "_Take Screenshot"
msgstr "_Fari ekrankopion"

#: ../vinagre/vinagre-ui.h:89
msgid "Take a screenshot of the current remote desktop"
msgstr ""

#: ../vinagre/vinagre-ui.h:93
msgid "View the current remote desktop in fullscreen mode"
msgstr ""

#: ../vinagre/vinagre-utils.vala:40
msgid "An error occurred"
msgstr "Eraro okazis"

#: ../vinagre/vinagre-utils.vala:84
msgid "Vinagre failed to open a UI file, with the error message:"
msgstr ""

#: ../vinagre/vinagre-utils.vala:85
msgid "Please check your installation."
msgstr "Bonvolu kontroli vian instalaĵon."

#: ../vinagre/vinagre-utils.vala:89
msgid "Error loading UI file"
msgstr ""

#. Translators: %s is a protocol, like VNC or SSH
#: ../vinagre/vinagre-utils.vala:112
#, c-format
msgid "%s authentication is required"
msgstr ""

#: ../vinagre/vinagre-utils.vala:194
msgid "Error showing help"
msgstr ""

#: ../vinagre/vinagre-window.c:390
#, c-format
msgid "Could not merge UI XML file: %s"
msgstr ""

#: ../vinagre/vinagre-window.c:421
msgid "_Recent Connections"
msgstr ""

#. Translators: This is server:port, a statusbar tooltip when mouse is over a bookmark item on menu
#: ../vinagre/vinagre-window.c:579
#, c-format
msgid "Open %s:%d"
msgstr "Malfermi %s:%d"

#: ../vinagre/vinagre-window.c:773
msgid ""
"Vinagre disables keyboard shortcuts by default, so that any keyboard "
"shortcuts are sent to the remote desktop.\n"
"\n"
"This message will appear only once."
msgstr ""

#: ../vinagre/vinagre-window.c:779
msgid "Enable shortcuts"
msgstr "Enŝalti klavkombinojn"

#: ../vinagre/vinagre-window.c:792 ../vinagre/vinagre-window.c:798
#, c-format
msgid "Error while creating the file %s: %s"
msgstr "Eraro dum kreado de la dosiero %s: %s"

#~ msgid "Active plugins"
#~ msgstr "Aktivaj kromprogramoj"

#~ msgid ""
#~ "List of active plugins. It contains the \"Location\" of the active "
#~ "plugins. See the .vinagre-plugin file for obtaining the \"Location\" of a "
#~ "given plugin."
#~ msgstr ""
#~ "Listo de aktivaj kromprogramoj. Ĝi enhavas la \"Lokon\" de aktivaj "
#~ "kromprogramoj. Vidu la dosieron .gedit-plugin por trovi la \"Lokon\" de "
#~ "specifa kromprogramo."

#~ msgid "Interface"
#~ msgstr "Interfaco"

#~ msgid "Preferences"
#~ msgstr "Agordoj"

#~ msgid "_Always show tabs"
#~ msgstr "Ĉi_am montri langetojn"

#~ msgid "IM Status"
#~ msgstr "IM-stato"

#~ msgid "_New folder"
#~ msgstr "_Nova dosierujo"

#~ msgid "Create a new folder"
#~ msgstr "Krei novan dosierujon"

#~ msgid "_Open bookmark"
#~ msgstr "_Malfermi legosignon"

#~ msgid "_Edit bookmark"
#~ msgstr "R_edakti legosignon"

#~ msgid "_Remove from bookmarks"
#~ msgstr "_Forigi de legosignoj"

#~ msgid "Invalid operation"
#~ msgstr "Nevalida operacio"

#~ msgid "Hide panel"
#~ msgstr "Kaŝi panelon"

#~ msgid "Connecting..."
#~ msgstr "Konektante..."

#~ msgid "Plugin Manager"
#~ msgstr "Kromprogram-administrilo"

#~ msgid "_Plugins"
#~ msgstr "_Kromprogramoj"

#~ msgid "Select active plugins"
#~ msgstr "Elekti aktivajn kromprogramojn"

#~ msgid "Side _Pane"
#~ msgstr "Flanka _panelo"

#~ msgid "Show or hide the side pane"
#~ msgstr "Montri aŭ kaŝi la flankan panelon"

#~ msgid "Vinagre Website"
#~ msgstr "Vinagreo-retpaĝaro"

#~ msgid "_About"
#~ msgstr "_Pri"

#~ msgid "Plugin"
#~ msgstr "Kromprogramo"

#~ msgid "Enabled"
#~ msgstr "Enŝaltite"

#~ msgid "C_onfigure"
#~ msgstr "Ag_ordi"

#~ msgid "A_ctivate"
#~ msgstr "A_ktivigi"

#~ msgid "Ac_tivate All"
#~ msgstr "Ak_tivigi ĉiujn"

#~ msgid "_Deactivate All"
#~ msgstr "_Malaktivigi ĉiujn"

#~ msgid "_About Plugin"
#~ msgstr "_Pri kromprogramo"

#~ msgid "C_onfigure Plugin"
#~ msgstr "Ag_ordi kromprogramon"

#~ msgid "C_lose All"
#~ msgstr "_Fermi ĉiujn"

#~ msgid ""
#~ "You should have received a copy of the GNU General Public License along "
#~ "with this program. If not, see <http://www.gnu.org/licenses/>."
#~ msgstr ""
#~ "Vi devintus ricevi kopion de la Ĝenerala Publika Permesilo de GNU kune "
#~ "kun ĉi tiu programo. Se ne, vidu <http://www.gnu.org/licenses/>."
